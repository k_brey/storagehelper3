<?php


namespace Piseth\Storagehelper;
use Illuminate\Support\ServiceProvider;


class StorageHelperServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->loadViewsFrom(__DIR__.'/resource/views', 'storagehelper');
        $this->loadRoutesFrom(__DIR__.'/route/web.php');
        $this->mergeConfigFrom(
            __DIR__.'/config/storagehelper.php', 'storagehelper'
        );
        $this->publishes([
            __DIR__.'/config/storagehelper.php' => config_path('storagehelper.php')
        ]);
    }

    public function register()
    {
        
    }
}