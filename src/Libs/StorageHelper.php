<?php


namespace Piseth\Storagehelper\Libs;


use phpDocumentor\Reflection\Types\This;
use Piseth\Storagehelper\DB\Prototype\IStorage;

trait StorageHelper
{
    private $attribute = null;
    private $fixedName = null;
    private $returnMessage = null;
    private $pathToStore = null;
    public function helpMeForFile($attribute)
    {
        $_newObject  = new self();
//        $this->attribute = $attribute;
        $_newObject->attribute = $attribute;
        return $_newObject;
    }

    public function fixedName($fixedName)
    {
        $this->fixedName = $fixedName;
        return $this;
    }

    public function pathToStore($pathToStore)
    {
        $this->pathToStore = $pathToStore;
        return $this;
    }
    public function ok()
    {
        try{
            if (isset($this->attribute)){
                if($this->pathToStore != null){
                    if($this->fixedName == null){
                        $this->fixedName = $this->attribute->getClientOriginalName();
                    }
                    $this->attribute->storeAs($this->pathToStore, $this->fixedName);
                    $this->returnMessage = $this->fixedName;
                }else{
                    $this->returnMessage = "path to store is ".$this->pathToStore;
                }
            }else{
                $this->returnMessage = "naf";
            }
            return $this->returnMessage;
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
    }
}